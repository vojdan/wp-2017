package mk.ukim.finki.wp.videoaggregator.web.controllers;

import mk.ukim.finki.wp.videoaggregator.config.thymeleaf.Layout;
import mk.ukim.finki.wp.videoaggregator.model.Category;
import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.service.CategoryService;
import mk.ukim.finki.wp.videoaggregator.service.ManageVideoService;
import mk.ukim.finki.wp.videoaggregator.service.SearchVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

/**
 * @author Riste Stojanov
 */
@Controller
public class LandingController {
  public static final String PAGE_ORDER_BY_CRITERIA = "title";

  private SearchVideoService videoService;
  private CategoryService categoryService;
  private ManageVideoService manageVideoService;


  @Autowired
  public LandingController(SearchVideoService videoService,
                           CategoryService categoryService,
                           ManageVideoService manageVideoService){
    this.videoService = videoService;
    this.categoryService = categoryService;
    this.manageVideoService = manageVideoService;
  }

  @Layout("layouts/master")
  @GetMapping("/")
  public String index(Model model, @RequestParam(required = false, defaultValue = "") String query, @RequestParam(required = false, defaultValue = "0") Integer page, @RequestParam(required = false, defaultValue = "1") Integer size) {
    Page<Video> videoList;
    if(query == null || query.isEmpty()) {
      videoList = videoService.findAll(getPageable(page, size));
    }
    else{
      videoList = videoService.findByTitle(getPageable(page,size), "%" + query + "%");
    }
    model.addAttribute("prevPageRequest", buildURIFromParams(query, videoList.isFirst() ? 0: videoList.getNumber() - 1, size));
    model.addAttribute("nextPageRequest", buildURIFromParams(query, videoList.isLast() ? videoList.getNumber() : videoList.getNumber() + 1, size));
    model.addAttribute("query", query);
    model.addAttribute("videos", videoList.getContent());
    model.addAttribute("pageNumber", videoList.getNumber());
    model.addAttribute("prevPage", videoList.isFirst() ? 0: videoList.getNumber() - 1);
    model.addAttribute("nextPage", videoList.isLast() ? videoList.getNumber() : videoList.getNumber() + 1);
    model.addAttribute("hasNext", videoList.hasNext());
    model.addAttribute("hasPrev", videoList.hasPrevious());
    return "fragments/contents";
  }

  @Layout("layouts/master")
  @GetMapping("/categories")
  public String categories() {
    return "fragments/category-list";
  }

  @Layout("layouts/master")
  @GetMapping("/category")
  public String category() {
    return "fragments/category-form";
  }

  @Layout("layouts/master")
  @GetMapping("/category/new")
  public String addCategory() {
    return "fragments/category-form";
  }

  @Layout("layouts/master")
  @GetMapping("/category/{id}/edit")
  public String editCategory(@PathVariable Long id, Model model) {
    String layout = "fragments/category-form";
    Optional<Category> category =  categoryService.findOne(id);
    if(category.isPresent()){
      model.addAttribute("cat", category.get());
    }
    else{
      layout = "fragments/error";
    }
    return layout;
  }

  @Layout("layouts/master")
  @GetMapping("/category/{categoryTitle}")
  public String categoryFeed(@PathVariable String categoryTitle, Model model) {
    Iterable<Video> videoList = videoService.findByCategoryTitle(categoryTitle);
    model.addAttribute("videos", videoList);
    return "fragments/contents";
  }

  @Layout("layouts/master")
  @GetMapping("/tag/{tagTitle}")
  public String tagFeed(@PathVariable String tagTitle, Model model) {
    Iterable<Video> videoList = videoService.findByTagName(tagTitle);
    model.addAttribute("videos", videoList);
    return "fragments/contents";
  }

  @Layout("layouts/master")
  @GetMapping("/video/create")
  public String video() {
    return "fragments/video-form";
  }

  @Layout("layouts/master")
  @GetMapping("/video/{videoId}")
  public String loadVideo(@PathVariable Long videoId, Model model) {
    String layout = "fragments/video-details";
    Optional<Video> video = videoService.findByIdWithTags(videoId);

    if(video.isPresent()){
      model.addAttribute("video", video.get());
    }
    else{
      layout = "fragments/error";
    }

    return layout;

  }

  @Layout("layouts/master")
  @GetMapping("/video/{videoId}/edit")
  public String editVideo(@PathVariable Long videoId, Model model) {
    String layout = "fragments/video-form";
    Optional<Video> video = videoService.findByIdWithTags(videoId);

    if(video.isPresent()){
      model.addAttribute("video", video.get());
    }
    else{
      layout = "fragments/error";
    }

    return layout;

  }

  private Pageable getPageable(Integer page, Integer size){
    return new PageRequest(page, size, new Sort(Sort.Direction.DESC, PAGE_ORDER_BY_CRITERIA));
  }

  public static String buildURIFromParams(String searchQuery, Integer page, Integer size){
    return "/?query=" + searchQuery + "&page=" + page + "&size=" + size;
  }

}
