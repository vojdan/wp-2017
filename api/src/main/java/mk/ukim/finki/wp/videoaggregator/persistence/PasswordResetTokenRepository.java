package mk.ukim.finki.wp.videoaggregator.persistence;

import mk.ukim.finki.wp.videoaggregator.model.PasswordResetToken;

public interface PasswordResetTokenRepository {

  void save(PasswordResetToken token);

  PasswordResetToken findByToken(String token);

  void delete(PasswordResetToken passwordResetToken);
}
