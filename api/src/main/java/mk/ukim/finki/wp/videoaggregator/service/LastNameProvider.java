package mk.ukim.finki.wp.videoaggregator.service;

/**
 * @author Riste Stojanov
 */
public interface LastNameProvider {

    String lastName(String name);
}
