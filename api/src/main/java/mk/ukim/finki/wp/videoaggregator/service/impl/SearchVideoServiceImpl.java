package mk.ukim.finki.wp.videoaggregator.service.impl;

import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.persistence.jpa.JpaVideoRepository;
import mk.ukim.finki.wp.videoaggregator.service.SearchVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.Optional;

/**
 * @author Riste Stojanov
 */
@Service
public class SearchVideoServiceImpl implements SearchVideoService {

  private final JpaVideoRepository videoRepository;

  @Autowired
  public SearchVideoServiceImpl(JpaVideoRepository videoRepository) {
    this.videoRepository = videoRepository;
  }

  @Override
  public Optional<Video> findByIdWithoutTags(Long id) {
    return videoRepository.findOne(id);
  }

  @Override
  public Optional<Video> findByIdWithTags(Long id) {
    return videoRepository.findOneVideoWithTags(id);
  }

  @Override
  public Page<Video> findAll(Pageable pageable) {
    return videoRepository.findAll(pageable);
  }

  @Override
  public Page<Video> findAll(Specification<Video> specification, Pageable pageable) {
    return videoRepository.findAll(specification, pageable);
  }

  @Override
  public Iterable<Video> findByCategoryTitle(String categoryTitle) {
    return videoRepository.findByCategoryTitle(categoryTitle);
  }

  @Override
  public Iterable<Video> findByTagName(String tagName) {
    return videoRepository.findByTagsName(tagName);
  }

  @Override
  public Page<Video> findByTitle(Pageable pageable, String title) {
    Specification<Video> titleLike = (root, query, cb) -> cb.like(root.get("title"), title);
    return videoRepository.findAll(titleLike, pageable);
  }

  @Override
  public Page<Video> findAll(Example<Video> example, Pageable pageable) {
    return videoRepository.findAll(example, pageable);
  }

}
