package mk.ukim.finki.wp.videoaggregator.persistence;

import mk.ukim.finki.wp.videoaggregator.model.Tag;

import java.util.Optional;

/**
 * @author Riste Stojanov
 */
public interface TagRepository {
    Optional<Tag> findOne(String tag);

    Tag save(Tag newTag);

    // the following method throws an exception
//    Iterable<Tag> findByTitle(String name);
}
