package mk.ukim.finki.wp.videoaggregator.web.rest;

import mk.ukim.finki.wp.videoaggregator.model.Video;
import mk.ukim.finki.wp.videoaggregator.service.SearchVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * @author Riste Stojanov
 */
@CrossOrigin("http://localhost:4200")
@RestController
@RequestMapping(value = "/api/videos", produces = MediaType.APPLICATION_JSON_VALUE)
public class SearchVideoController {

  private final SearchVideoService searchVideoService;

  @Autowired
  public SearchVideoController(SearchVideoService searchVideoService) {
    this.searchVideoService = searchVideoService;
  }

  @RequestMapping(method = RequestMethod.GET)
  public Iterable<Video> all(@RequestParam(required = false) String title) {
//    if(title==null || title.isEmpty()) {
//      return searchVideoService.findAll((getPageable(offset, limit, orderBy)));
//    } else {
//      return searchVideoService.findByTitle(title);
//    }
    return null;
  }
}
