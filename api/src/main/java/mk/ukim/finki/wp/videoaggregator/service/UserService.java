package mk.ukim.finki.wp.videoaggregator.service;

import mk.ukim.finki.wp.videoaggregator.model.User;
import mk.ukim.finki.wp.videoaggregator.model.UserDto;
import mk.ukim.finki.wp.videoaggregator.model.UserVerificationToken;
import mk.ukim.finki.wp.videoaggregator.model.exceptions.DuplicateEmailException;

import java.util.Optional;

public interface UserService {

  User registerNewUser(UserDto user) throws DuplicateEmailException;

  User saveUser(UserDto user);

  User save(User user);

  User getUser(String token);

  UserVerificationToken getToken(String token);

  void createVerificationToken(User user, String randomToken);

  UserVerificationToken generateNewVerificationToken(String verificationToken);

  String validateVerificationToken(String token) ;

  User findByUsername(String username);

  Optional<User> findByEmail(String email);

  User findById(Long id);

  void createPasswordResetTokenForUser(User user, String token);

  String validatePasswordResetToken(Long id, String token);

  void changeUserPassword(User user, String password);

  long numberOfUsers();

}
