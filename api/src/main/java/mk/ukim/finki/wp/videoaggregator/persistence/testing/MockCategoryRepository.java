package mk.ukim.finki.wp.videoaggregator.persistence.testing;

import mk.ukim.finki.wp.videoaggregator.model.Category;
import mk.ukim.finki.wp.videoaggregator.persistence.CategoryRepository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Stream;

public class MockCategoryRepository implements CategoryRepository {
    @Override
    public Optional<Category> findOne(Long categoryId) {
        Category category = new Category();
        category.id = categoryId;
        category.title = "cat title";
        return Optional.of(category);
    }

    @Override
    public Category save(Category category) {
        return null;
    }

  @Override
  public Iterable<Category> findAll() {
    return new ArrayList<>();
  }

  @Override
  public Iterable<Category> findByTitle(String title) {
    return null;
  }

  @Override
  public Iterable<Category> findByTitleLike(String title) {
    return null;
  }

  @Override
  public void delete(long id) {

  }

  @Override
  public void softDelete(long id) {

  }

  @Override
  public long count() {
    return 0;
  }
}
