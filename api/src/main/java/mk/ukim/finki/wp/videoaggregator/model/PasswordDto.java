package mk.ukim.finki.wp.videoaggregator.model;

import lombok.Getter;
import lombok.Setter;
import mk.ukim.finki.wp.videoaggregator.web.validators.PasswordMatches;

@PasswordMatches
@Getter @Setter
public class PasswordDto {

  public String password;

  public String matchingPassword;

}
