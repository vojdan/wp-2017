package mk.ukim.finki.wp.videoaggregator.web.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import mk.ukim.finki.wp.videoaggregator.model.Tag;

import java.io.IOException;
import java.util.List;

/**
 * @author Riste Stojanov
 */
public class TagListSerializer extends JsonSerializer<List<Tag>> {
  @Override
  public void serialize(List<Tag> value,
                        JsonGenerator gen,
                        SerializerProvider serializers) throws IOException, JsonProcessingException {

    gen.writeStartArray();
    for (Tag tag : value) {
      gen.writeString(tag.name);
    }
    gen.writeEndArray();
  }
}
