import {Pipe, PipeTransform} from '@angular/core';

/**
 * The solution of the bug presented at the lecture is presented by:
 * Војдан Ќорвезироски
 *
 * The solution is documented at: https://angular.io/guide/pipes#pure-pipes
 */
@Pipe({
  name: 'filter',
  pure: false
})
export class FilterPipe implements PipeTransform {
  transform(items: string[], term: string): any {
    if (!term) {
      term = '';
    }
    if (!items) {
      return [];
    }
    return items.filter(item => item.indexOf(term) !== -1);
  }
}
