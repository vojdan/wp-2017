import {Component, Input, OnInit} from '@angular/core';
import {Video} from '../../model/Video';
import {ActivatedRoute} from '@angular/router';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import {VideoServiceInterface} from '../services/VideoServiceInterface';

@Component({
  selector: 'app-video-details',
  templateUrl: './video-details.component.html',
  styleUrls: ['./video-details.component.css']
})
export class VideoDetailsComponent implements OnInit {

  @Input('inputVideo')
  public video: Video;

  constructor(private route: ActivatedRoute,
              private  service: VideoServiceInterface) {
  }

  ngOnInit() {
    const videoTitle = this.route.snapshot.paramMap.get('title');

    this.service.findByTitle(videoTitle)
      .subscribe(videos => {
          if (videos.length > 0) {
            console.log('details for:', videos);
            this.video = videos[0];
          }
        },
        error => {
          console.error(error.errorMessage);
        });
  }

}
