import {Directive} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, Validator} from '@angular/forms';
import {isUrl} from './is-url.function';


@Directive({
  selector: '[isUrl]',
  providers: [
    // register the template-driven forms validators
    {
      // standard angular validators
      provide: NG_VALIDATORS,
      // use this exact instance
      useExisting: IsUrlDirective,
      multi: true
    }]
})
export class IsUrlDirective implements Validator {

  validate(control: AbstractControl): { [key: string]: any } {
    return isUrl()(control);
  }

}
